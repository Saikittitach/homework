import { StepperSelectionEvent } from '@angular/cdk/stepper';
import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatCheckboxChange } from '@angular/material/checkbox';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'homework';

  personalForm: FormGroup;
  isEditable = false;
  ageCollection: string[] = ['< 30', '31-50', '> 51'];
  countryOption: string[] = ['thailand', 'brazil', 'vietnam', 'france', 'china', 'japan', 'italy'];
  seletedCountry = [];
  checked = [];

  get firstname() {
    return this.personalForm.get('firstname').value
  }
  get lastname() {
    return this.personalForm.get('lastname').value
  }
  get email() {
    return this.personalForm.get('email').value
  }
  get gender() {
    return this.personalForm.get('gender').value
  }
  get age() {
    return this.personalForm.get('age').value
  }
  get country() {
    return this.personalForm.get('country').value
  }
  get spicy() {
    return this.personalForm.get('spicy').value
  }

  constructor() {
    this.personalForm = new FormGroup({
      firstname: new FormControl('', [Validators.required]),
      lastname: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.pattern('^[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}')]),
      gender: new FormControl('', [Validators.required]),
      age: new FormControl('', [Validators.required]),
      country: new FormControl('', [Validators.required]),
      spicy: new FormControl('', [Validators.required]),
    });
  }

  ngOnInit() {}

  onChange = (event: MatCheckboxChange, country: string): void => {
    if (event.checked) {
      this.seletedCountry.push(country);
      this.seletedCountry.filter((item) => {
        if (item == country) {
          return;
        }
      });
      this.personalForm.get('country').patchValue(this.seletedCountry);
      return;
    }
    let index = this.seletedCountry.indexOf(country);
    this.seletedCountry.splice(index, 1);
    this.personalForm.get('country').patchValue(this.seletedCountry);
  };

  selectionChange = (event: StepperSelectionEvent): void => {
    console.log(this.personalForm.value);
  };

  formatLabel(value: number) {
    return value;
  }

  resetForm = () => {
    this.personalForm.reset()
  }
}
